
import netscape.javascript.JSObject;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

public class Main {

    public static void main(String[] args) {

        JSONObject json = new JSONObject();

        json.put("Krzysiek ", "Programista ");
        json.put("age ", new Integer(28));

        JSONArray list = new JSONArray();
        list.add("Krzychu ");
        list.add("jem ");
        list.add("zupę ");

        json.put("messages ", list);

        try (FileWriter file2 = new FileWriter("Zapis")) {

            file2.write(json.toJSONString());
            file2.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(json);


        JSONParser jsonParser = new JSONParser();

        try {
            Object obj2 = jsonParser.parse(new FileReader("Zapis"));

            JSONObject jsonObject = (JSONObject) obj2;
            System.out.println(jsonObject);

            String name = (String) jsonObject.get("Krzysiek ");
            System.out.println(name);

            Integer age = (Integer) jsonObject.get(28);
            System.out.println(age);


            JSONArray msg = (JSONArray) jsonObject.get("messages ");
            Iterator<String> iterator = msg.iterator();

            while (iterator.hasNext()) {
                System.out.println(iterator.next());
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    }

