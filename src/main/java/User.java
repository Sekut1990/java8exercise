import java.util.Objects;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class User {

    Optional<Optional<String>> firstName= Optional.empty();
    String lastName;

    Logger LOG= Logger.getLogger(User.class.getName());

    public User(Optional<String> firstName, String lastName) {
        this.firstName = Optional.ofNullable(firstName);
        this.lastName = lastName;
        LOG.log(Level.INFO, "User created: " + this.toString());
    }

    public Optional<Optional<String>> getFirstName() {
        return firstName;
    }

    public void setFirstName(Optional<Optional<String>> firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
