import java.io.*;

public class Copylines {

    BufferedReader inputStream = null;
    PrintWriter outputStream = null;

    public void czytaj() throws IOException {
        try {
            inputStream = new BufferedReader(new FileReader("xanadu.txt"));
            outputStream = new PrintWriter(new FileWriter("characteroutput.txt"));

            String one;

            while ((one = inputStream.readLine()) != null) {
                System.out.println(one);
                outputStream.println(one);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(inputStream != null){
                inputStream.close();
            }
            if (outputStream !=null){
                outputStream.close();
            }
        }

    }
}
